import * as React from 'react';

const Dashboard = () => {
    return (
        <div id="wrapper">
            <div className="navbar-custom">
                <div className="logo-box">
                    <a href="index.html" className="logo text-center">
                        <span className="logo-lg">
                            <img src="assets/images/logo.png" alt="" />
                        </span>
                        <span className="logo-sm">
                            <img src="assets/images/logo.png" alt="" height="28" />
                        </span>
                    </a>
                </div>
                <ul className="list-unstyled topnav-menu topnav-menu-left m-0">
                    <li>
                        <button className="button-menu-mobile waves-effect waves-light">
                            <i className="fe-menu"></i>
                        </button>
                    </li>
                </ul>
            </div>
            <div className="left-side-menu">
                <div className="slimscroll-menu">
                    <div id="sidebar-menu">
                        <ul className="metismenu" id="side-menu">
                            <li>
                                <a href="index.html">
                                    <i className="fe-airplay"></i>
                                    <span> Dashboard </span>
                                </a>
                            </li>

                            <li>
                                <a href="javascript: void(0);">
                                    <i className="fe-briefcase"></i>
                                    <span> Your Outlet </span>
                                    <span className="menu-arrow"></span>
                                </a>
                                <ul className="nav-second-level" aria-expanded="false">
                                    <li><a href="#">Basic Informations</a></li>
                                    <li><a href="#">Availability Settings</a></li>
                                    <li><a href="#">Operational Settings</a></li>
                                </ul>
                            </li>

                            <li>
                                <a href="javascript: void(0);">
                                    <i className="fe-briefcase"></i>
                                    <span> Product Setup </span>
                                    <span className="menu-arrow"></span>
                                </a>
                                <ul className="nav-second-level" aria-expanded="false">
                                    <li><a href="#">Basic Informations</a></li>
                                    <li><a href="#">Availability Settings</a></li>
                                    <li><a href="#">Operational Settings</a></li>
                                </ul>
                            </li>

                            <li>
                                <a href="javascript: void(0);">
                                    <i className="fe-briefcase"></i>
                                    <span> Payment Gateway </span>
                                    <span className="menu-arrow"></span>
                                </a>
                                <ul className="nav-second-level" aria-expanded="false">
                                    <li><a href="#">Basic Informations</a></li>
                                    <li><a href="#">Availability Settings</a></li>
                                    <li><a href="#">Operational Settings</a></li>
                                </ul>
                            </li>

                            <li>
                                <a href="javascript: void(0);">
                                    <i className="fe-briefcase"></i>
                                    <span> Subscriptions</span>
                                    <span className="menu-arrow"></span>
                                </a>
                                <ul className="nav-second-level" aria-expanded="false">
                                    <li><a href="#">Basic Informations</a></li>
                                    <li><a href="#">Availability Settings</a></li>
                                    <li><a href="#">Operational Settings</a></li>
                                </ul>
                            </li>

                            <li>
                                <a href="#">
                                    <i className="fe-briefcase"></i>
                                    <span> Privacy Settings </span>
                                </a>
                            </li>
                            <li>
                                <a href="registration.html">
                                    <i className="fe-briefcase"></i>
                                    <span> Registration Steps </span>
                                </a>
                            </li>

                        </ul>
                    </div>
                    <div className="clearfix"></div>
                </div>

            </div>
            <div className="content-page">
                <div className="content dashboard">
                    <section className="dashboard-topsec">
                        <div className="row">
                            <div className="col-xl-5 dashboard-left">
                                <h3 className="title">Welcome to NinjaOs</h3>
                                <p>Let's finalize the details to prepare your ninjaos ordering system</p>
                                <div className="card">
                                    <div className="card-body">
                                        <h5 className="card-title no-margin">Congratulations</h5>
                                        <p className="no-margin">Your backend panel is ready. please check below. </p>
                                        <p className="no-margin">Open Backend</p>

                                        <h5>User Name: admin</h5>
                                        <h5>Password: admin@123</h5>

                                        <p className="no-margin">Let us know if you need some help</p>
                                        <a href="#" className="btn btn-dark mt-1">Contact Support</a>
                                    </div>
                                </div>
                            </div>
                            <div className="col-xl-7 dashboard-right">
                                <div className="row pb-1">
                                    <div className="col-9 head-text">
                                        <h3>Get ready for your first sale.</h3>
                                        <h3>Follow our tips to get started.</h3>
                                    </div>
                                    <div className="col-3">
                                        <div className="status-percentage">
                                            <span className="status-progress">75%</span>
                                            <div className="progress progress-md">
                                                <div className="progress-bar bg-success" role="progressbar" style={{ width: '75%', height: '20px' }}></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="card mt-2">
                                    <div className="card-body">
                                        <h5 className="card-title">Complete your pending setup and get started</h5>
                                        <div className="setup-steps">
                                            <div className="setup-item completed">
                                                <div className="leftimg">
                                                </div>
                                                <div className="right-text">
                                                    <h5 className="no-margin">Done - <span className="text-muted">Create your account</span> </h5>
                                                </div>
                                            </div>

                                            <div className="setup-item completed">
                                                <div className="leftimg">
                                                </div>
                                                <div className="right-text">
                                                    <h5 className="no-margin">Add Your Outlet</h5>
                                                    <p className="no-margin">Lorem Ipsum is simply dummy text of the printing
                                                and typesetting industry.</p>
                                                </div>
                                            </div>
                                            <div className="setup-item completed">
                                                <div className="leftimg">
                                                </div>
                                                <div className="right-text">
                                                    <h5 className="no-margin">Add Your Product</h5>
                                                    <p className="no-margin">Lorem ipsum dolor sit amet, consectetur adipiscing
                                                    elit, sed do eiusmod tempor incididunt ut labore et dolore magna
                                                aliqua.</p>
                                                </div>
                                            </div>
                                            <div className="setup-item completed active">
                                                <div className="leftimg">
                                                </div>
                                                <div className="right-text">
                                                    <h5 className="no-margin">Select Your Payment Gateway</h5>
                                                    <p className="no-margin">Lorem ipsum dolor sit amet, consectetur adipiscing
                                                    elit, sed do eiusmod tempor incididunt ut labore et dolore magna
                                                aliqua.</p>
                                                </div>
                                            </div>
                                            <div className="setup-item">
                                                <div className="leftimg">
                                                </div>
                                                <div className="right-text">
                                                    <h5 className="no-margin">Complete Your Subscriptions</h5>
                                                    <p className="no-margin">Lorem ipsum dolor sit amet, consectetur adipiscing
                                                    elit, sed do eiusmod tempor incididunt ut labore et dolore magna
                                                aliqua.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <div className="container-fluid mm-pad">
                        <div className="row">
                            <div className="col-xl-5">
                                <div className="card border-secondary special-promotion">
                                    <div className="card-body ">
                                        <h4 className="header-title">Special promotion for dine in feature</h4>
                                        <p>
                                            We’ve got your back at the every step of your e-commerce journey. Our Support
                                            Team is standing by to answer any question you may have and takes your feedback
                                    at heart.</p>
                                        <a href="#" className="btn btn-mmprimary">Contact Support</a>
                                    </div>
                                </div>
                                <div className="card border-secondary get-help">
                                    <div className="card-body ">
                                        <h4 className="header-title">Get help when you need it</h4>
                                        <p>
                                            We’ve got your back at the every step of your e-commerce journey. Our Support
                                            Team is standing by to answer any question you may have and takes your feedback
                                    at heart.</p>
                                        <a href="#" className="btn btn-primary">Contact Support</a>
                                    </div>
                                </div>
                            </div>
                            <div className="col-xl-7">
                                <div className="setup-videos">
                                    <h4 className="header-title">Tips for your setup</h4>
                                    <p>Check below if you need some tips for the setup</p>
                                    <div className="row">
                                        <div className="col-6">
                                            <iframe className="card-img"
                                                src="https://www.youtube.com/embed/tgbNymZ7vqY?playlist=tgbNymZ7vqY&loop=1">
                                            </iframe>
                                        </div>
                                        <div className="col-6">
                                            <iframe className="card-img"
                                                src="https://www.youtube.com/embed/tgbNymZ7vqY?playlist=tgbNymZ7vqY&loop=1">
                                            </iframe>
                                        </div>
                                    </div>
                                </div>
                                <div className="dashboard-blogs mt-3">
                                    <h4 className="header-title mt-2">Blog & Recommended Reading</h4>
                                    <p>Check below if you need some tips for the setup</p>

                                    <div className="row">
                                        <div className="col-6">
                                            <div className="card">
                                                <img className="card-img-top img-fluid" src="assets/images/small/img-4.jpg"
                                                    alt="Card image cap" />
                                                <div className="card-body">
                                                    <h5 className="card-title">Card title</h5>
                                                    <p className="card-text">This is a longer card with supporting text below as
                                                    a natural lead-in to additional content. This content is a little
                                                bit longer.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-6">
                                            <div className="card">
                                                <img className="card-img-top img-fluid" src="assets/images/small/img-4.jpg"
                                                    alt="Card image cap" />
                                                <div className="card-body">
                                                    <h5 className="card-title">Card title</h5>
                                                    <p className="card-text">This is a longer card with supporting text below as
                                                    a natural lead-in to additional content. This content is a little
                                                bit longer.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <footer className="footer">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-md-12">
                                Do you have any question ? Let us know
                    </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
    );
}
export default Dashboard;